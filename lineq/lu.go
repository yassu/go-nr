package lineq

import (
	// "fmt"
	. "gitlab.com/yassu/go-nr/entity"
)

// LU分解を用いて 連立方程式を解く
func SolveByLU(mat Mat, b Vec) (sol Vec) {
	l, u := LU(mat)
	return SolveLU(l, u, b)
}

// LU分解が分かっているときに その連立方程式を解く
func SolveLU(l, u Mat, b Vec) Vec {
	sol := solveL(l, b)
	sol = solveU(u, sol)

	return sol
}

// 行列の逆行列を求める.
// アルゴリズムとしてはLU分解を用いる.
func MatInv(mat Mat) Mat {
	size := len(mat)
	inv := ZeroMat(size, size)
	b := ZeroVec(size)

	for i := 0; i < size; i++ {
		b[i] = 1.0
		inv.UpdateCol(i, SolveByLU(mat, b))
		b[i] = 0.0
	}

	return inv
}

// 行列の行列式を求める.
// アルゴリズムとしてはLU分解を用いる.
func Det(mat Mat) float64 {
	size := len(mat)
	l, u := LU(mat)
	var det float64 = 1.0

	for i := 0; i < size; i++ {
		det *= l[i][i]
	}

	for i := 0; i < size; i++ {
		det *= u[i][i]
	}

	return det
}

// LU分解をする関数
// ピポット選択はしない
func LU(mat Mat) (l Mat, u Mat) {
	size := len(mat)
	l = make([][]float64, size)
	u = make([][]float64, size)

	// lとuの初期化
	for i := 0; i < size; i++ {
		l[i] = make([]float64, size)
		u[i] = make([]float64, size)
	}

	// 以下 Crout法を行う
	for i := 0; i < size; i++ {
		l[i][i] = 1.0
	}

	for j := 0; j < size; j++ {
		for i := 0; i <= j; i++ {
			u[i][j] = mat[i][j]
			for k := 0; k < i; k++ {
				u[i][j] -= l[i][k] * u[k][j]
			}
		}

		for i := j + 1; i < size; i++ {
			l[i][j] = mat[i][j]
			for k := 0; k < j; k++ {
				l[i][j] -= l[i][k] * u[k][j]
			}
			l[i][j] /= u[j][j]
		}
	}

	return
}

// 方程式 mat * x = b に関する反修改良
//
// matのLU分解が分かっているとする.
func UpdatedSolLU(mat Mat, l Mat, u Mat, b Vec, sol Vec) Vec {
	delX := SolveLU(l, u, mat.Act(sol).Sub(b))

	return sol.Sub(delX)
}

// 連立方程式l * x = bを解く.
// ここで l は下三角行列であることを仮定する.
// この関数はLU分解を用いて 連立方程式を解く際に利用する.
func solveL(l Mat, b Vec) Vec {
	size := len(b)
	sol := ZeroVec(size)

	for i := 0; i < size; i++ {
		sol[i] = b[i]
		for k := 0; k < i; k++ {
			sol[i] -= l[i][k] * sol[k]
		}
		sol[i] /= l[i][i]
	}

	return sol
}

// 連立方程式u * x = bを解く.
// ここで l は上三角行列であることを仮定する.
// この関数はLU分解を用いて 連立方程式を解く際に利用する.
func solveU(u Mat, b Vec) Vec {
	size := len(b)
	sol := ZeroVec(size)

	for i := size - 1; i >= 0; i-- {
		sol[i] = b[i]
		for k := i + 1; k < size; k++ {
			sol[i] -= u[i][k] * sol[k]
		}
		sol[i] /= u[i][i]
	}

	return sol
}
