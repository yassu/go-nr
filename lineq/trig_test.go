package lineq

import (
	"reflect"
	"testing"

	. "gitlab.com/yassu/go-nr/entity"
)

func Test_NewTrigMat(t *testing.T) {
	a := []float64{0.0, 2.0, 3.0}
	b := []float64{5.0, 7.0, 11.0}
	c := []float64{13.0, 17.0, 0.0}

	got := NewTrigMat(a, b, c)
	want := Mat([][]float64{
		{5.0, 13.0, 0.0},
		{2.0, 7.0, 17.0},
		{0.0, 3.0, 11.0},
	})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_LUTrig(t *testing.T) {
	a := []float64{0.0, 2.0, 3.0}
	b := []float64{5.0, 7.0, 11.0}
	c := []float64{13.0, 17.0, 0.0}
	mat := NewTrigMat(a, b, c)
	l, u := LUTrig(mat)

	if !l.Prod(u).Close(mat) {
		t.Errorf("l: %v", l)
		t.Errorf("u: %v", u)
		t.Errorf("mat: %v", mat)
	}
}
