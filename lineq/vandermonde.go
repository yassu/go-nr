package lineq

import (
	. "gitlab.com/yassu/go-nr/entity"
)

func NewVandermonde(xs []float64) Mat {
	size := len(xs)
	mat := ZeroMat(size, size)

	for y := 0; y < size; y++ {
		for x := 0; x < size; x++ {
			if y == 0 {
				mat[y][x] = 1.0
			} else {
				mat[y][x] = xs[x] * mat[y-1][x]
			}
		}
	}

	return Mat(mat)
}

// 式 2.8.2で定義される行列の逆行列を求める
func VandermondeInv(mat Mat) Mat {
	size := len(mat)

	if size == 1 {
		return Mat([][]float64{{1.0}})
	}

	xs := make([]float64, size)

	for i := 0; i < size; i++ {
		xs[i] = mat[1][i]
	}
	parent := parentLagPoly(xs)

	inv := make([][]float64, size)
	// copy parent to inv
	for i := 0; i < size; i++ {
		inv[i] = make([]float64, size+1)
		copy(inv[i], parent)
	}

	for j := 0; j < size; j++ {
		for n := 0; n < size; n++ {
			if n == j {
				inv[j], _ = divPoly1(inv[j], xs[j])
			}
		}
	}

	for j := 0; j < size; j++ {
		for n := 0; n < size; n++ {
			if n == j {
				continue
			}
			for k := 0; k < size; k++ {
				inv[j][k] /= (xs[j] - xs[n])
			}
		}
	}

	return inv
}

func parentLagPoly(xs []float64) []float64 {
	p := make([]float64, len(xs)+1)
	p[0] = 1.0

	for n, x := range xs {
		newP := make([]float64, len(p))
		copy(newP, p)

		for j := 0; j < n+2; j++ {
			if j == 0 {
				newP[0] = p[0]
			} else if j == n+1 {
				newP[n+1] = -x * p[n]
			} else {
				newP[j] = p[j] - x*p[j-1]
			}
		}
		p = newP
	}

	// reverse
	size := len(p)
	newP := make([]float64, size)
	for i := 0; i < size; i++ {
		newP[i] = p[size-1-i]
	}

	return newP
}

// pをx-aで割った時の商と余りを返す.
func divPoly1(p []float64, a float64) ([]float64, float64) {
	size := len(p)
	r := make([]float64, size)
	copy(r, p)
	q := make([]float64, size-1)

	for j := size - 1; j >= 1; j-- {
		q[j-1] = r[j]
		r[j-1] += a * r[j]
		r[j] = 0.0
	}

	return q, r[0]
}
