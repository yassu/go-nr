package lineq

import (
	// "fmt"
	. "gitlab.com/yassu/go-nr/entity"
)

// Toeplitz行列を作成する.
//
// rsの長さは奇数であることを仮定する.
func NewToeplitz(rs []float64) Mat {
	size := len(rs)/2 + 1

	mat := ZeroMat(size, size)

	for y := 0; y < size; y++ {
		for x := 0; x < size; x++ {
			mat[y][x] = rs[y-x+size-1]
		}
	}

	return mat
}

// 線形Toeplitz問題を解く.
//
// TODO: 行列のサイズが3以上の場合にバグが出る.
// おそらく gMatかhMatの計算にバグがある.
func SolveToeplitz(mat Mat, b Vec) Vec {
	// Rの初期化
	size := len(b)
	rSize := 2*size - 1
	var rs = make([]float64, 0, rSize)
	for i := 0; i < size; i++ {
		rs = append(rs, mat[0][size-1-i])
	}
	for i := 1; i < size; i++ {
		rs = append(rs, mat[i][0])
	}

	// Xmatの初期化
	xMat := make([][]float64, size)
	for i := 0; i < size; i++ {
		xMat[i] = make([]float64, i+1)
	}
	xMat[0][0] = b[0] / rs[size-1]

	// Gmatの初期化
	gMat := make([][]float64, size-1)
	for i := 0; i < size-1; i++ {
		gMat[i] = make([]float64, i+1)
	}
	gMat[0][0] = rs[-1+size-1] / rs[size-1]

	// Hmatの初期化
	hMat := make([][]float64, size-1)
	for i := 0; i < size-1; i++ {
		hMat[i] = make([]float64, i+1)
	}
	hMat[0][0] = rs[1+size-1] / rs[size-1]

	{
		for m := 0; m < size-2; m++ {
			// gMatの計算
			{
				// gMat[m+1][m+1]を計算
				{
					numeratorS := 0.0
					denomS := 0.0
					for j := 0; j <= m; j++ {
						numeratorS += rs[j-m-1+size-1] * gMat[m][j]
						denomS += rs[j-m-1+size-1] * hMat[m][m+1-(j+1)]
					}

					gMat[m+1][m+1] = (numeratorS - b[-m-1+size-1]) / (denomS - rs[size-1])
				}

				// hMat[m+1][m+1]を計算
				{
					numeratorS := 0.0
					denomS := 0.0
					for j := 0; j <= m; j++ {
						numeratorS += rs[m+1-j+size-1] * hMat[m][j]
						denomS += rs[m+1-j+size-1] * gMat[m][m+1-(j+1)]
					}

					hMat[m+1][m+1] = (numeratorS - b[m+1]) / (denomS - rs[size-1])
				}

				// gMat[m+1][j]を計算
				for j := 0; j < m+1; j++ {
					gMat[m+1][j] = gMat[m][j] - gMat[m+1][m+1]*hMat[m][m+1-j-1]
				}
			}

			// hMatの計算
			{
				// hMat[m+1][j]を計算
				for j := 0; j < m+1; j++ {
					hMat[m+1][j] = hMat[m][j] - hMat[m+1][m+1]*gMat[m][m+1-j-1]
				}
			}
		}
	}

	for m := 0; m < size-1; m++ {
		// x_{M+1}^{M+1}を計算
		// xMat[m+1][m+1]を計算
		{
			numeratorS := 0.0
			denomS := 0.0
			for j := 0; j <= m; j++ {
				numeratorS += rs[m+1-j+size-1] * xMat[m][j]
				denomS += rs[m+1-j+size-1] * gMat[m][m+1-(j+1)]
			}

			xMat[m+1][m+1] = (numeratorS - b[m+1]) / (denomS - rs[size-1])
		}
		// xMat[m+1][M+1-j](j > 0)を計算
		for j := 1; j < m+2; j++ {
			xMat[m+1][m+1-j] = xMat[m][m+1-j] - xMat[m+1][m+1]*gMat[m][j-1]
		}
	}

	// Xmat, Gmat, Hmatを漸化式に基づいて更新
	return xMat[size-1]
}
