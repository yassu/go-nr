// 第二章: 連立一次方程式の解法
package lineq

import (
	. "gitlab.com/yassu/go-nr/entity"
)

// 2.1 Gauss-Jordan法の実装
// 表している連立方程式の式の本数と変数の数は一致しているとする
// 現状ピボットの選択なし
func SolveGJordan(mat Mat, b Vec) (sol Vec) {
	mat = CopyMat(mat)
	b = CopyVec(b)
	size := len(mat)

	for i := 0; i < size; i++ {
		a := mat[i][i]
		for k := 0; k < size; k++ {
			mat[i][k] /= a
		}
		b[i] /= a

		// j != iのとき mat[j][k]をmat[j][k] - mat[j][i] * mat[i][k]にする
		for j := 0; j < size; j++ {
			if i == j {
				continue
			}

			a := mat[j][i]

			for k := 0; k < size; k++ {
				mat[j][k] -= a * mat[i][k]
			}
			b[j] -= a * b[i]
		}
	}
	return b
}

// 2.2 Gauss-Jordan法 + 交代代入の実装
// 表している連立方程式の式の本数と変数の数は一致しているとする
// 現状ピボットの選択なし
func SolveGJordan2(mat Mat, b Vec) (sol Vec) {
	mat = CopyMat(mat)
	b = CopyVec(b)
	size := len(mat)

	for i := 0; i < size; i++ {
		a := mat[i][i]
		for k := 0; k < size; k++ {
			mat[i][k] /= a
		}
		b[i] /= a

		// j != iのとき mat[j][k]をmat[j][k] - mat[j][i] * mat[i][k]にする
		for j := i + 1; j < size; j++ {

			a := mat[j][i]

			for k := 0; k < size; k++ {
				mat[j][k] -= a * mat[i][k]
			}
			b[j] -= a * b[i]
		}
	}

	sol = ZeroVec(size)
	if size > 0 {
		sol[size-1] = b[size-1] / mat[size-1][size-1]
	}

	for i := size - 2; i >= 0; i-- {
		sol[i] = b[i]
		for j := i + 1; j < size; j++ {
			sol[i] -= mat[i][j] * sol[j]
		}
	}

	return sol
}
