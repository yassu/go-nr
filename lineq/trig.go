package lineq

import (
	// "fmt"
	. "gitlab.com/yassu/go-nr/entity"
)

// 対角成分の一つ下, 対角成分, 対角成分のベクトルを用いて三重対角な行列を作成する.
//
// 作成された行列matは条件
//
// mat[i][i] = b[i], mat[i][i-1] = a[i], mat[i][i+1] = c[i]
//
// を満たす(本のP62にある形式).
// ただし aの最後の要素及びcの最初の要素は0.0であるとする.
func NewTrigMat(a, b, c []float64) Mat {
	size := len(b)
	mat := ZeroMat(size, size)

	for i := 0; i < size; i++ {
		if i != 0 {
			mat[i][i-1] = a[i]
		}
		mat[i][i] = b[i]
		if i != size-1 {
			mat[i][i+1] = c[i]
		}
	}

	return mat
}

// NewTrigMatで作成した行列と同じ形式の行列に対して
// LU分解を行う.
//
// 行列の作成にO(n^2)かかり, L及びUの計算にO(n)かかるので
// この関数の計算にはO(n^2)かかる.
func LUTrig(mat Mat) (l, u Mat) {
	size := len(mat)
	l = E(size)
	u = ZeroMat(size, size)

	for i := 0; i < size; i++ {
		if i > 0 {
			u[i-1][i] = mat[i-1][i]
		}

		if i == 0 {
			u[i][i] = mat[i][i]
		} else {
			u[i][i] = mat[i][i] - l[i][i-1]*u[i-1][i]
		}

		if i != size-1 {
			l[i+1][i] = mat[i+1][i] / u[i][i]
		}
	}

	return l, u
}
