package lineq

import (
	"reflect"
	"testing"

	. "gitlab.com/yassu/go-nr/entity"
)

// LU分解を用いて連立方程式を解くテスト
// 式の数: 1, 変数の数: 1
func Test_SolveByLU(t *testing.T) {
	mat := [][]float64{{2.0}}
	b := []float64{1.0}
	got := SolveByLU(mat, b)
	want := []float64{0.5}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// LU分解を用いて連立方程式を解くテスト
// 式の数: 2, 変数の数: 2
func Test_SolveByLU2(t *testing.T) {
	mat := [][]float64{
		{2.0, 1.0},
		{1.0, 2.0},
	}
	b := []float64{5.0, 4.0}
	got := SolveByLU(mat, b)
	want := []float64{2.0, 1.0}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// LU分解を用いて連立方程式を解くテスト
// 式の数: 3, 変数の数: 3
func Test_SolveByLU3(t *testing.T) {
	mat := [][]float64{
		{1.0, 2.0, 2.0},
		{2.0, 3.0, 2.0},
		{5.0, 3.0, 3.0},
	}
	b := []float64{3.0, 1.0, -6.0}
	got := SolveByLU(mat, b)
	want := []float64{-3.0, 1.0, 2.0}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// LU分解を用いて連立方程式を解くテスト
// 式の数: 300, 変数の数: 300
func Test_SolveByLU4(t *testing.T) {
	size := 300
	mat := RandMat(size, size)
	b := RandVec(size)
	res := SolveByLU(mat, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Raise Error")
	}
}

func Test_MatInv(t *testing.T) {
	mat := Mat([][]float64{{2.0}})
	got := MatInv(mat)
	want := Mat([][]float64{{0.5}})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_MatInv2(t *testing.T) {
	mat := Mat([][]float64{{2.0, 3.0}, {5.0, 7.0}})
	got := MatInv(mat)
	want := Mat([][]float64{{-7.0, 3.0}, {5.0, -2.0}})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_MatInv3(t *testing.T) {
	mat := Mat([][]float64{{2.0, 3.0, 5.0}, {7.0, 11.0, 13.0}, {17.0, 19.0, 23.0}})
	res := MatInv(mat)

	if !mat.Prod(res).Close(E(3)) {
		t.Errorf("mat: %v", mat)
		t.Errorf("res: %v", res)
		t.Errorf("mat * res: %v", mat.Prod(res))
	}
}

func Test_MatInv4(t *testing.T) {
	size := 10
	mat := RandMat(size, size)
	res := MatInv(mat)

	if !mat.Prod(res).Close(E(size)) {
		t.Errorf("mat: %v", mat)
		t.Errorf("res: %v", res)
		t.Errorf("mat * res: %v", mat.Prod(res))
	}
}

func Test_Det(t *testing.T) {
	mat := Mat([][]float64{{2.0, 3.0, 5.0}, {7.0, 11.0, 13.0}, {17.0, 19.0, 23.0}})
	got := Det(mat)
	want := -78.0

	if got != want {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_Det2(t *testing.T) {
	mat := Mat([][]float64{{2.0, 3.0}, {5.0, 7.0}})
	got := Det(mat)
	want := -1.0

	if got != want {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// LUが分かっているときに連立方程式を解くテスト
// 式の数: 1, 変数の数: 1
func Test_SolveLU(t *testing.T) {
	l := Mat([][]float64{{1.0}})
	u := Mat([][]float64{{3.0}})
	b := Vec([]float64{4.0})
	mat := l.Prod(u)
	res := SolveLU(l, u, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Bad solution. res=%v", res)
	}
}

// LUが分かっているときに連立方程式を解くテスト
// 式の数: 2, 変数の数: 2
func Test_SolveLU2(t *testing.T) {
	l := Mat([][]float64{
		{2.0, 0.0},
		{3.0, 5.0},
	})
	u := Mat([][]float64{
		{2.0, 3.0},
		{0.0, 5.0},
	})
	b := Vec([]float64{7.0, 11.0})
	mat := l.Prod(u)
	res := SolveLU(l, u, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Bad solution. res=%v", res)
	}
}

// LUが分かっているときに連立方程式を解くテスト
// 式の数: 3, 変数の数: 3
func Test_SolveLU3(t *testing.T) {
	l := Mat([][]float64{
		{2.0, 0.0, 0.0},
		{3.0, 5.0, 0.0},
		{7.0, 11.0, 13.0},
	})
	u := Mat([][]float64{
		{17.0, 23.0, 29.0},
		{0.0, 31.0, 37.0},
		{0.0, 0.0, 41.0},
	})
	b := Vec([]float64{47.0, 53.0, 57.0})
	mat := l.Prod(u)
	res := SolveLU(l, u, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Bad solution. res=%v", res)
	}
}

// 1 * 1行列のLU分解のテスト
func Test_LU(t *testing.T) {
	mat, _ := NewMat([][]float64{{1.0}})

	gotL, gotU := LU(mat)
	wantL, _ := NewMat([][]float64{{1.0}})
	wantU, _ := NewMat([][]float64{{1.0}})

	if !gotL.Close(wantL) {
		t.Errorf("gotL: %v", gotL)
		t.Errorf("wantL: %v", wantL)
	}

	if !gotU.Close(wantU) {
		t.Errorf("gotU: %v", gotU)
		t.Errorf("wantU: %v", wantU)
	}
}

// 2 * 2行列のLU分解のテスト
func Test_LU2(t *testing.T) {
	mat, _ := NewMat([][]float64{{2.0, 0.0}, {0.0, 3.0}})

	gotL, gotU := LU(mat)
	wantL, _ := NewMat([][]float64{{1.0, 0.0}, {0.0, 1.0}})
	wantU, _ := NewMat([][]float64{{2.0, 0.0}, {0.0, 3.0}})

	if !gotL.Close(wantL) {
		t.Errorf("gotL: %v", gotL)
		t.Errorf("wantL: %v", wantL)
	}

	if !gotU.Close(wantU) {
		t.Errorf("gotU: %v", gotU)
		t.Errorf("wantU: %v", wantU)
	}
}

// 3 * 3行列のLU分解のテスト
// http://ocw.nagoya-u.jp/files/58/05.pdf
// に掲載されている例
func Test_LU3(t *testing.T) {
	mat, _ := NewMat([][]float64{
		{2.0, 3.0, -1.0},
		{4.0, 4.0, -3.0},
		{-2.0, 3.0, -1.0},
	})

	gotL, gotU := LU(mat)
	wantL := Mat([][]float64{
		{1.0, 0.0, 0.0},
		{2.0, 1.0, 0.0},
		{-1.0, -3.0, 1.0},
	})
	wantU := Mat([][]float64{
		{2.0, 3.0, -1.0},
		{0.0, -2.0, -1.0},
		{0.0, 0.0, -5.0},
	})

	if !gotL.Close(wantL) {
		t.Errorf("gotL: %v", gotL)
		t.Errorf("wantL: %v", wantL)
	}

	if !gotU.Close(wantU) {
		t.Errorf("gotU: %v", gotU)
		t.Errorf("wantU: %v", wantU)
	}
}

// 4 * 4行列のLU分解のテスト
// http://ocw.nagoya-u.jp/files/58/05.pdf
// に掲載されている例
func Test_LU4(t *testing.T) {
	mat, _ := NewMat([][]float64{
		{1.0, 1.0, 0.0, 3.0},
		{2.0, 1.0, -1.0, 1.0},
		{3.0, -1.0, -1.0, 2.0},
		{-1.0, 2.0, 3.0, -1.0},
	})

	gotL, gotU := LU(mat)
	wantL := Mat([][]float64{
		{1.0, 0.0, 0.0, 0.0},
		{2.0, 1.0, 0.0, 0.0},
		{3.0, 4.0, 1.0, 0.0},
		{-1.0, -3.0, 0.0, 1.0},
	})
	wantU := Mat([][]float64{
		{1.0, 1.0, 0.0, 3.0},
		{0.0, -1.0, -1.0, -5.0},
		{0.0, 0.0, 3.0, 13.0},
		{0.0, 0.0, 0.0, -13.0},
	})

	if !gotL.Close(wantL) {
		t.Errorf("gotL: %v", gotL)
		t.Errorf("wantL: %v", wantL)
	}

	if !gotU.Close(wantU) {
		t.Errorf("gotU: %v", gotU)
		t.Errorf("wantU: %v", wantU)
	}
}

func Test_UpdatedSolLU(t *testing.T) {
	size := 300
	mat := RandMat(size, size)
	b := RandVec(size)

	l, u := LU(mat)
	sol := SolveLU(l, u, b)
	sol2 := UpdatedSolLU(mat, l, u, b, sol)

	if b.Dist2(mat.Act(sol)) < b.Dist2(mat.Act(sol2)) {
		t.Errorf("Bad: err1=%v, err2=%v",
			b.Dist2(mat.Act(sol)),
			b.Dist2(mat.Act(sol2)))
	}
}

func Test_solveL0(t *testing.T) {
	mat := Mat([][]float64{})
	b := Vec([]float64{})

	got := solveL(mat, b)
	want := Vec([]float64{})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Error: got=%v", got)
		t.Errorf("Error: want=%v", want)
	}
}

func Test_solveL1(t *testing.T) {
	mat := Mat([][]float64{{2.0}})
	b := Vec([]float64{3.0})

	got := solveL(mat, b)
	want := Vec([]float64{1.5})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Error: got=%v", got)
		t.Errorf("Error: want=%v", want)
	}
}

func Test_solveL2(t *testing.T) {
	mat := Mat([][]float64{
		{2.0, 0.0},
		{3.0, 4.0}})
	b := Vec([]float64{5.0, 6.0})

	got := solveL(mat, b)
	want := Vec([]float64{5.0 / 2.0, -3.0 / 8.0})

	if !got.Close(want) {
		t.Errorf("Error: got=%v", got)
		t.Errorf("Error: want=%v", want)
	}
}

func Test_solveL3(t *testing.T) {
	mat := Mat([][]float64{
		{2.0, 0.0, 0.0},
		{3.0, 4.0, 0.0},
		{5.0, 6.0, 7.0},
	})
	b := Vec([]float64{8.0, 9.0, 10.0})

	got := solveL(mat, b)
	want := Vec([]float64{4.0, -3.0 / 4.0, -11.0 / 14.0})

	if !got.Close(want) {
		t.Errorf("Error: got=%v", got)
		t.Errorf("Error: want=%v", want)
	}
}

func Test_solveL4(t *testing.T) {
	mat := Mat([][]float64{
		{2.0, 0.0, 0.0, 0.0},
		{3.0, 4.0, 0.0, 0.0},
		{5.0, 6.0, 7.0, 0.0},
		{8.0, 9.0, 10.0, 11.0},
	})
	b := Vec([]float64{12.0, 13.0, 14.0, 15.0})

	res := solveL(mat, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Bad solution. res=%v", res)
	}
}

func Test_solveU0(t *testing.T) {
	mat := Mat([][]float64{})
	b := Vec([]float64{})

	got := solveU(mat, b)
	want := Vec([]float64{})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Error: got=%v", got)
		t.Errorf("Error: want=%v", want)
	}
}

func Test_solveU1(t *testing.T) {
	mat := Mat([][]float64{{2.0}})
	b := Vec([]float64{3.0})

	got := solveU(mat, b)
	want := Vec([]float64{1.5})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Error: got=%v", got)
		t.Errorf("Error: want=%v", want)
	}
}

func Test_solveU2(t *testing.T) {
	mat := Mat([][]float64{
		{1.0, 2.0},
		{0.0, 3.0}})
	b := Vec([]float64{4.0, 5.0})

	got := solveU(mat, b)
	want := Vec([]float64{2.0 / 3.0, 5.0 / 3.0})

	if !got.Close(want) {
		t.Errorf("Error: got=%v", got)
		t.Errorf("Error: want=%v", want)
	}
}

func Test_solveU3(t *testing.T) {
	mat := Mat([][]float64{
		{1.0, 2.0, 3.0},
		{0.0, 4.0, 5.0},
		{0.0, 0.0, 6.0}})
	b := Vec([]float64{7.0, 8.0, 11.0})

	res := solveU(mat, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Bad solution. res=%v", res)
	}
}

func Test_solveU4(t *testing.T) {
	mat := Mat([][]float64{
		{1.0, 2.0, 3.0, 4.0},
		{0.0, 5.0, 6.0, 7.0},
		{0.0, 0.0, 8.0, 9.0},
		{0.0, 0.0, 0.0, 10.0}})
	b := Vec([]float64{11.0, 12.0, 13.0, 14.0})

	res := solveU(mat, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Bad solution. res=%v", res)
	}
}
