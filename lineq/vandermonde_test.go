package lineq

import (
	"math/rand"
	"testing"

	. "gitlab.com/yassu/go-nr/entity"
)

func Test_NewVandermonde(t *testing.T) {
	got := NewVandermonde([]float64{})
	want := Mat([][]float64{})

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_NewVandermonde2(t *testing.T) {
	got := NewVandermonde([]float64{3.0})
	want := Mat([][]float64{{1.0}})

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_NewVandermonde3(t *testing.T) {
	got := NewVandermonde([]float64{2.0, 4.0, 9.0})
	want := Mat([][]float64{
		{1.0, 1.0, 1.0},
		{2.0, 4.0, 9.0},
		{4.0, 16.0, 81.0},
	})

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_VandermondeInv(t *testing.T) {
	mat := NewVandermonde([]float64{2.0, 4.0})

	got := VandermondeInv(mat)
	want := Mat([][]float64{
		{2.0, -0.5},
		{-1.0, 0.5},
	})

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_VandermondeInv2(t *testing.T) {
	mat := NewVandermonde([]float64{})

	got := VandermondeInv(mat)
	want := Mat([][]float64{})

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_VandermondeInv3(t *testing.T) {
	mat := NewVandermonde([]float64{2.0})

	got := VandermondeInv(mat)
	want := Mat([][]float64{{1.0}})

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_VandermondeInv4(t *testing.T) {
	size := 10
	xs := make([]float64, size)
	for i := 0; i < size; i++ {
		xs[i] = rand.Float64()
	}

	mat := NewVandermonde(xs)
	inv := VandermondeInv(mat)

	if !mat.Prod(inv).Close(E(size)) {
		t.Errorf("Illegal Answer")
		t.Errorf("mat=%v", mat)
		t.Errorf("inv=%v", inv)
	}
}

func Test_parentLagPoly(t *testing.T) {
	xs := []float64{-1.0, 1.0, 2.0}
	got := parentLagPoly(xs)
	// 2 - x - 2 * x * x + x * x * x
	want := []float64{2.0, -1.0, -2.0, 1.0}

	if !CloseS(got, want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_divPoly1(t *testing.T) {
	cases := []struct {
		f     []float64
		a     float64
		wantQ []float64
		wantR float64
	}{
		{
			f:     []float64{2.0, -3.0, 1.0},
			a:     2.0,
			wantQ: []float64{-1.0, 1.0},
			wantR: 0.0,
		},
		{
			f:     []float64{5.0, -7.0, 6.0},
			a:     3.0,
			wantQ: []float64{11.0, 6.0},
			wantR: 38.0,
		},
	}

	for _, c := range cases {
		gotQ, gotR := divPoly1(c.f, c.a)
		if !CloseS(gotQ, c.wantQ) {
			t.Errorf("gotQ: %v", gotQ)
			t.Errorf("wantQ: %v", c.wantQ)
		}

		if !Close(gotR, c.wantR) {
			t.Errorf("gotR: %v", gotR)
			t.Errorf("wantR: %v", c.wantR)
		}
	}
}
