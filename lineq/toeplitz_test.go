package lineq

import (
	"testing"

	. "gitlab.com/yassu/go-nr/entity"
)

func Test_NewToeplitz1(t *testing.T) {
	got := NewToeplitz([]float64{2.0})
	want := Mat([][]float64{{2.0}})

	if !got.Close(want) {
		t.Errorf("Fail: got=%v want=%v", got, want)
	}
}

func Test_NewToeplitz3(t *testing.T) {
	got := NewToeplitz([]float64{2.0, 3.0, 5.0})
	want := Mat([][]float64{
		{3.0, 2.0},
		{5.0, 3.0},
	})

	if !got.Close(want) {
		t.Errorf("Fail: got=%v want=%v", got, want)
	}
}

// func Test_SolveToeplitz(t *testing.T) {
// 	mat := Mat([][]float64{})
// 	b := Vec([]float64{})
// 	got := SolveToeplitz(mat, b)
// 	want := Vec([]float64{})
//
// 	if !got.Close(want) {
// 		t.Errorf("Fail: got=%v want=%v", got, want)
// 	}
// }

func Test_SolveToeplitz2(t *testing.T) {
	mat := Mat([][]float64{
		{3.0, 2.0},
		{5.0, 3.0},
	})
	b := Vec([]float64{7.0, 11.0})
	got := SolveToeplitz(mat, b)
	want := Vec([]float64{1.0, 2.0})

	if !got.Close(want) {
		t.Errorf("Fail: got=%v want=%v", got, want)
	}
}

// func Test_SolveToeplitz3(t *testing.T) {
// 	mat := NewToeplitz(RandVec(5))
// 	b := RandVec(3)
// 	got := SolveToeplitz(mat, b)
//
// 	if !mat.Act(got).Close(b) {
// 		t.Errorf("Fail: mat=%v got=%v b=%v", mat, got, b)
// 	}
// }
