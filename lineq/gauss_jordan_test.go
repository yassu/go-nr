package lineq

import (
	"testing"

	. "gitlab.com/yassu/go-nr/entity"
)

// ガウスジョルダン法のテスト
// 式の数: 1, 変数の数: 1
func Test_GaussJordan1(t *testing.T) {
	mat := [][]float64{{2.0}}
	b := []float64{1.0}
	got := SolveGJordan(mat, b)
	want := []float64{0.5}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// ガウスジョルダン法のテスト
// 式の数: 2, 変数の数: 2
func Test_GaussJordan2(t *testing.T) {
	mat := [][]float64{
		{2.0, 1.0},
		{1.0, 2.0},
	}
	b := []float64{5.0, 4.0}
	got := SolveGJordan(mat, b)
	want := []float64{2.0, 1.0}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// ガウスジョルダン法のテスト
// 式の数: 3, 変数の数: 3
func Test_GaussJordan3(t *testing.T) {
	mat := [][]float64{
		{1.0, 2.0, 2.0},
		{2.0, 3.0, 2.0},
		{5.0, 3.0, 3.0},
	}
	b := []float64{3.0, 1.0, -6.0}
	got := SolveGJordan(mat, b)
	want := []float64{-3.0, 1.0, 2.0}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// ガウスジョルダン法のテスト
// 式の数: 300, 変数の数: 300
func Test_GaussJordan4(t *testing.T) {
	size := 300
	mat := RandMat(size, size)
	b := RandVec(size)
	res := SolveGJordan2(mat, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Raise Error")
	}
}

// ガウスジョルダン法+後退代入のテスト
// 式の数: 1, 変数の数: 1
func Test_GaussJordan2_1(t *testing.T) {
	mat := [][]float64{{2.0}}
	b := []float64{1.0}
	got := SolveGJordan2(mat, b)
	want := []float64{0.5}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// ガウスジョルダン法+後退代入のテスト
// 式の数: 2, 変数の数: 2
func Test_GaussJordan2_2(t *testing.T) {
	mat := [][]float64{
		{2.0, 1.0},
		{1.0, 2.0},
	}
	b := []float64{5.0, 4.0}
	got := SolveGJordan2(mat, b)
	want := []float64{2.0, 1.0}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// ガウスジョルダン法+後退代入のテスト
// 式の数: 3, 変数の数: 3
func Test_GaussJordan2_3(t *testing.T) {
	mat := [][]float64{
		{1.0, 2.0, 2.0},
		{2.0, 3.0, 2.0},
		{5.0, 3.0, 3.0},
	}
	b := []float64{3.0, 1.0, -6.0}
	got := SolveGJordan2(mat, b)
	want := []float64{-3.0, 1.0, 2.0}

	if !got.Close(want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

// ガウスジョルダン法+後退代入のテスト
// 式の数: 300, 変数の数: 300
func Test_GaussJordan2_4(t *testing.T) {
	size := 300
	mat := RandMat(size, size)
	b := RandVec(size)
	res := SolveGJordan2(mat, b)

	if !mat.Act(res).Close(b) {
		t.Errorf("Raise Error")
	}
}
