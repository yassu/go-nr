package interpolation

import (
	"testing"

	. "gitlab.com/yassu/go-nr/entity"
)

func Test_Nevile(t *testing.T) {
	cases := []struct {
		data   map[float64]float64
		inputs []float64
		wants  []float64
	}{
		{ // 8(0次多項式)
			data: map[float64]float64{
				2: 8.0,
			},
			inputs: []float64{-2.0, 5.0},
			wants:  []float64{8.0, 8.0},
		},
		{ // x + 6(1次多項式)
			data:   map[float64]float64{2.0: 8.0, 4.0: 10.0},
			inputs: []float64{-2.0, 3.0},
			wants:  []float64{4.0, 9.0},
		},
		{ // x^2 + x + 1
			data: map[float64]float64{
				1.0:  3.0,
				2.0:  7.0,
				-1.0: 1.0,
			},
			inputs: []float64{0.5, -3.0},
			wants:  []float64{1.75, 7.0},
		},
		{ // x^3 + 2*x^2 + 3*x + 4
			data: map[float64]float64{
				-2.0: -2.0,
				-1.0: 2.0,
				1.0:  10.0,
				2.0:  26.0,
			},
			inputs: []float64{0.0, 3.0},
			wants:  []float64{4.0, 58.0},
		},
	}

	for _, c := range cases {
		f := Nevile(c.data)
		for i, input := range c.inputs {
			got := f(input)
			if !Close(got, c.wants[i]) {
				t.Errorf("i=%v,", i)
				t.Errorf("want=%v", c.wants[i])
				t.Errorf("got=%v", got)
			}
		}
	}
}
