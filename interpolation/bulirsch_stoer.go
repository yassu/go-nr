package interpolation

import (
	"fmt"
)

// TODO: 動かない
func BulirschStoer(data map[float64]float64) func(float64) float64 {

	size := len(data)
	xs := make([]float64, size)
	ys := make([]float64, size)
	i := 0
	for x, y := range data {
		xs[i] = x
		ys[i] = y
		i++
	}

	return func(x float64) float64 {
		fmt.Println(16, x)
		if _, contain := data[x]; contain {
			return data[x]
		}
		fmt.Println(20, data)

		rMat := make([][]float64, size)
		for i, _ := range xs {
			rMat[i] = make([]float64, size-i)
		}

		for i := 0; i < size; i++ {
			rMat[i][0] = ys[i]
		}

		for m := 1; m < size; m++ {
			for i := 0; i < size-m; i++ {
				if m == 1 {
					rMat[i][m] = ys[i]
				} else {
					rMat[i][m] = rMat[i+1][m-1] +
						(rMat[i+1][m-1]-rMat[i][m-1])/((x-xs[i])/(x-xs[i+m])*
							(1-(rMat[i+1][m-1]-rMat[i][m-2])/(rMat[i+1][m-1]-rMat[i+1][m-2]))-
							1)
				}
			}
		}

		return rMat[0][size-1]
	}
}
