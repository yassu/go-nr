package interpolation

import (
	"testing"

	. "gitlab.com/yassu/go-nr/entity"
)

// 与えたデータでそれぞれの値になっていることのテスト
func Test_BulirschStoer(t *testing.T) {
	cases := []struct {
		data   map[float64]float64
		inputs []float64
		wants  []float64
	}{
		{ // 8(0次多項式)
			data: map[float64]float64{
				2.0: 3.0,
				5.0: 7.0,
			},
			inputs: []float64{2.0, 5.0},
			wants:  []float64{3.0, 7.0},
		},
	}

	for _, c := range cases {
		f := BulirschStoer(c.data)
		for i, input := range c.inputs {
			got := f(input)
			if !Close(got, c.wants[i]) {
				t.Errorf("i=%v,", i)
				t.Errorf("want=%v", c.wants[i])
				t.Errorf("got=%v", got)
			}
		}
	}
}

// 連続性のテスト
func Test_BulirschStoer2(t *testing.T) {
	cases := []struct {
		data   map[float64]float64
		inputs []float64
		eps    float64
	}{
		{ // 8(0次多項式)
			data: map[float64]float64{
				2.0: 3.0,
				5.0: 7.0,
			},
			inputs: []float64{2.0, 5.0},
			eps:    1e-6,
		},
	}

	for _, c := range cases {
		f := BulirschStoer(c.data)
		for i, input := range c.inputs {
			got := f(input)
			gotPlus := f(input + c.eps)

			if !Close(got, gotPlus) {
				t.Errorf("i=%v", i)
				t.Errorf("got=%v", got)
				t.Errorf("gotPlus=%v", gotPlus)
			}
		}
	}
}
