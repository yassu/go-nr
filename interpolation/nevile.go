// 第三章: 補間と補外
package interpolation

import (
// "fmt"
)

// 3.1の多項式による補間・補外のNevileのアルゴリズムを実装する.
//
// TODO: C, Dを用いるようにする.
func Nevile(data map[float64]float64) func(float64) float64 {
	size := len(data)
	xs := make([]float64, size)
	ys := make([]float64, size)

	i := 0
	for x, y := range data {
		xs[i] = x
		ys[i] = y
		i++
	}

	return func(x float64) float64 {
		pMat := make([][]float64, size)
		for i, _ := range xs {
			pMat[i] = make([]float64, size-i)
		}

		for i := 0; i < size; i++ {
			pMat[i][0] = ys[i]
		}

		for j := 0; j < size; j++ {
			for i := 0; i < size-j-1; i++ {
				pMat[i][j+1] = ((x-xs[i+j+1])*pMat[i][j] + (xs[i]-x)*pMat[i+1][j]) / (xs[i] - xs[i+j+1])
			}
		}
		return pMat[0][size-1]
	}
}
