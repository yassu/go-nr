go-nr
=======

[![Build Status](https://gitlab.com/yassu/go-nr/badges/master/pipeline.svg)](https://gitlab.com/yassu/go-nr/pipelines/latest)


[ニューメリカルレシピ・イン・シー 日本語版―C言語による数値計算のレシピ](https://www.amazon.co.jp/dp/4874085601/)の勉強用のリポジトリ.

Go言語で実装しながらやる.

[ドキュメント](https://godoc.org/gitlab.com/yassu/go-nr)

## 開発メモ

特定の関数のdocを見るコマンド:

```
$ go doc gitlab.com/yassu/go-nr/fib
$ go doc gitlab.com/yassu/go-nr/fib Fib
```

License
=========

[Apache 2.0](https://gitlab.com/yassu/go-nr/-/raw/master/LICENSE.txt)
