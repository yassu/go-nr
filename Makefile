BIN := go-nr
export GO111MODULE=on

.PHONY: all
all: clean build

.PHONY: build
pdf:
	curl https://e-maxx.ru/bookz/files/numerical_recipes.pdf -o nr.pdf

.PHONY: build
build:
	go build

.PHONY: clean
clean:
	rm -rf $(BIN)
	go clean
	go clean -testcache


.PHONY: test
test: build
	go test -v ./... -cover -count=1

.PHONY: run
run: build
	go run main.go

.PHONY: docs
docs:
	godoc -http=:6060
