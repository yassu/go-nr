package main

import (
	"fmt"

	. "gitlab.com/yassu/go-nr/entity"
	"gitlab.com/yassu/go-nr/lineq"
)

func main() {
	mat, _ := NewMat([][]float64{
		{1.0, 1.0, 0.0, 3.0},
		{2.0, 1.0, -1.0, 1.0},
		{3.0, -1.0, -1.0, 2.0},
		{-1.0, 2.0, 3.0, -1.0},
	})

	gotL, gotU := lineq.LU(mat)

	fmt.Println(gotL)
	fmt.Println(gotU)
}
