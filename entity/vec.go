package entity

import (
	"math/rand"
)

type Vec []float64

// ベクトルを作成する.
func NewVec(v []float64) Vec {
	return Vec(v)
}

// ベクトルselfとベクトルotherの差を返す.
func (self Vec) Sub(other Vec) Vec {
	size := len(self)

	v := make([]float64, size)
	for i := 0; i < size; i++ {
		v[i] = self[i] - other[i]
	}

	return Vec(v)
}

// ベクトルselfとベクトルotherのそれぞれの要素の差の二乗の和を返す.
func (self Vec) Dist2(other Vec) float64 {
	size := len(self)

	var s float64
	for i := 0; i < size; i++ {
		s += (self[i] - other[i]) * (self[i] - other[i])
	}

	return s
}

// ベクトルselfとベクトルotherのそれぞれの要素が1.0e-4より小さいかどうかを返す.
func (self Vec) Close(other Vec) bool {
	return CloseS(self, other)
}

// 長さがsizeのベクトルを返す.
func ZeroVec(size int) Vec {
	v := make([]float64, size)
	return Vec(v)
}

// ベクトルのコピーを作成する.
func CopyVec(v Vec) Vec {
	size := len(v)
	dest := make([]float64, size)
	copy(dest, v)

	return Vec(dest)
}

// 長さがsizeのベクトルを作成する.
//
// それぞれの要素は(0.0, 1.0)の中に存在するランダムな値である.
func RandVec(size int) Vec {
	v := make([]float64, size)
	for i := 0; i < size; i++ {
		v[i] = rand.Float64()
	}
	return Vec(v)
}
