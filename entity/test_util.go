package entity

import (
	"math"
)

// 二つの浮動小数点数の差の絶対値が1.0e-4より小さいかどうかを返す.
func Close(f1, f2 float64) bool {
	return math.Abs(f1-f2) < 1e-4
}

// 二つの浮動小数点数のスライスに対して,
// それぞれの要素の差の絶対値が1.0e-4より小さいかどうかを返す.
func CloseS(fs1, fs2 []float64) bool {
	if len(fs1) != len(fs2) {
		return false
	}

	l := len(fs1)
	for i := 0; i < l; i++ {
		if !Close(fs1[i], fs2[i]) {
			return false
		}
	}

	return true
}
