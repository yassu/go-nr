package entity

import (
	"fmt"
	"reflect"
	"testing"
)

func Test_NewMat(t *testing.T) {
	matData := [][]float64{
		{1.0, 2.0, 3.0},
		{4.0, 5.0, 6.0},
		{7.0, 8.0, 9.0},
	}
	mat, err := NewMat(matData)
	if err != nil {
		t.Errorf("err != nil")
		t.Errorf("mat = %v", mat)
	}

	if mat == nil {
		t.Errorf("mat == nil")
		t.Errorf("err = %v", err)
	}
}

func Test_NewMat2(t *testing.T) {
	matData := [][]float64{
		{1.0, 2.0, 3.0},
		{4.0, 5.0, 6.0},
		{7.0, 8.0},
	}
	mat, err := NewMat(matData)
	if err == nil {
		t.Errorf("err == nil")
		t.Errorf("mat = %v", mat)
	}

	if len(mat) != 0 {
		t.Errorf("mat is not empty")
		t.Errorf("err = %v", err)
	}
}

func ExampleNewMat() {
	mat := Mat([][]float64{
		{1.0, 2.0},
		{3.0, 4.0},
	})
	fmt.Println(mat[0][1])
	// Output: 2
}

func Test_UpdateCol(t *testing.T) {
	mat := Mat([][]float64{
		{1.0, 2.0, 3.0, 4.0},
		{5.0, 6.0, 7.0, 8.0},
		{9.0, 10.0, 11.0, 12.0},
	})
	v := Vec([]float64{100.0, 101.0, 102.0})
	mat.UpdateCol(2, v)

	want := Mat([][]float64{
		{1.0, 2.0, 100.0, 4.0},
		{5.0, 6.0, 101.0, 8.0},
		{9.0, 10.0, 102.0, 12.0},
	})

	if !reflect.DeepEqual(mat, want) {
		t.Errorf("mat: %v", mat)
		t.Errorf("want: %v", want)
	}
}

func Test_Mat_Prod(t *testing.T) {
	mat1 := Mat([][]float64{
		{2.0, 3.0, 5.0},
		{7.0, 11.0, 13.0},
	})
	mat2 := Mat([][]float64{
		{17.0, 19.0, 23.0, 29.0},
		{31.0, 37.0, 41.0, 43.0},
		{47.0, 53.0, 59.0, 61.0},
	})
	mat := mat1.Prod(mat2)

	got00 := mat[0][0]
	got12 := mat[1][2]
	gotShape := [2]int{len(mat), len(mat[0])}
	want00 := 2.0*17.0 + 3.0*31.0 + 5.0*47.0
	want12 := 7.0*23.0 + 11.0*41.0 + 13.0*59.0
	wantShape := [2]int{2, 4}

	if got00 != want00 {
		t.Errorf("got00: %v", got00)
		t.Errorf("want00: %v", want00)
	}

	if got12 != want12 {
		t.Errorf("got12: %v", got12)
		t.Errorf("want12: %v", want12)
	}

	if gotShape != wantShape {
		t.Errorf("gotShape: %v", gotShape)
		t.Errorf("wantShape: %v", wantShape)
	}
}

func Test_Mat_Act(t *testing.T) {
	mat := Mat([][]float64{
		{1.0, 2.0, 3.0},
		{4.0, 5.0, 6.0},
	})
	v := Vec([]float64{7, 8, 9})

	got := mat.Act(v)
	want := Vec([]float64{
		1*7 + 2*8 + 3*9,
		4*7 + 5*8 + 6*9,
	})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func ExampleMat_Act() {
	mat := Mat([][]float64{{1, 2}, {3, 4}})
	v := Vec([]float64{5, 6})
	res := mat.Act(v)

	fmt.Println(res)
	// Output: [17 39]
}

func Test_Mat_Dist2(t *testing.T) {
	mat1 := Mat([][]float64{
		{1.0, 2.0, 3.0},
		{4.0, 6.0, 9.0},
		{10.0, 11.0, 12.0},
	})

	mat2 := Mat([][]float64{
		{1.0, 4.0, 3.0},
		{5.0, 6.0, 9.0},
		{10.0, 11.0, 15.0},
	})

	got := mat1.Dist2(mat2)
	want := 14.0

	if got != want {
		t.Errorf("got got: %v", got)
		t.Errorf("want want: %v", want)
	}
}

func Test_E(t *testing.T) {
	got := E(3)
	want := Mat([][]float64{
		{1.0, 0.0, 0.0},
		{0.0, 1.0, 0.0},
		{0.0, 0.0, 1.0},
	})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func ExampleE() {
	mat := E(4)
	fmt.Println(mat)
	// Output: [[1 0 0 0] [0 1 0 0] [0 0 1 0] [0 0 0 1]]
}

func Test_validateMat(t *testing.T) {
	mat := [][]float64{
		{1.0, 2.0, 3.0},
		{4.0, 5.0, 6.0},
		{7.0, 8.0, 9.0},
	}
	got := validateMat(mat)
	var want error = nil

	if got != want {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_validateMat2(t *testing.T) {
	mat := [][]float64{
		{1.0, 2.0, 3.0},
		{4.0, 5.0, 6.0},
	}
	got := validateMat(mat)
	var want error = nil

	if got != want {
		t.Errorf("got: %v", got)
		t.Errorf("want: %v", want)
	}
}

func Test_validateMat3(t *testing.T) {
	mat := [][]float64{
		{1.0, 2.0, 3.0},
		{4.0, 5.0, 6.0},
		{7.0, 8.0},
	}
	got := validateMat(mat)

	if got == nil {
		t.Errorf("got: %v", got)
	}
}

func Test_validateMat4(t *testing.T) {
	mat := [][]float64{
		{1.0, 2.0},
		{4.0, 5.0, 6.0},
		{7.0, 8.0, 9.0},
	}
	got := validateMat(mat)

	if got == nil {
		t.Errorf("got: %v", got)
	}
}

func Test_ZeroMat(t *testing.T) {
	got := ZeroMat(2, 3)
	want := Mat{
		{0.0, 0.0, 0.0},
		{0.0, 0.0, 0.0},
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %T: %v", got, got)
		t.Errorf("want: %T: %v", want, want)
	}
}

func ExampleZeroMat() {
	mat := ZeroMat(2, 3)
	fmt.Println(mat)
	// Output: [[0 0 0] [0 0 0]]
}

func Test_CopyMat(t *testing.T) {
	mat, _ := NewMat([][]float64{
		{1.0, 2.0},
		{3.0, 4.0},
	})
	mat2 := CopyMat(mat)
	mat2[0][1] = 100.0

	want_mat, _ := NewMat([][]float64{
		{1.0, 2.0},
		{3.0, 4.0},
	})
	want_mat2, _ := NewMat([][]float64{
		{1.0, 100.0},
		{3.0, 4.0},
	})

	if !reflect.DeepEqual(mat, want_mat) {
		t.Errorf("got mat: %T: %v", mat, mat)
		t.Errorf("want mat: %T: %v", want_mat, want_mat)
	}
	if !reflect.DeepEqual(mat2, want_mat2) {
		t.Errorf("got mat2: %T: %v", mat2, mat2)
		t.Errorf("want mat2: %T: %v", want_mat2, want_mat2)
	}
}

func Test_RandMat(t *testing.T) {
	mat := RandMat(2, 3)

	for y := 0; y < 2; y++ {
		for x := 0; x < 3; x++ {
			if mat[y][x] <= 0 || mat[y][x] >= 1 {
				t.Errorf("Error: y=%v x=%v val=%v", y, x, mat[y][x])
			}
		}
	}
}
