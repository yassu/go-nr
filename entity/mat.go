package entity

import (
	"fmt"
	"math/rand"
)

type Mat [][]float64

// 行列を作成する.
func NewMat(mat [][]float64) (Mat, error) {
	err := validateMat(mat)
	if err == nil {
		return Mat(mat), nil
	} else {
		return Mat{}, err
	}
}

// 行列selfのcolIdx列目をベクトルvで更新する.
func (self Mat) UpdateCol(colIdx int, v Vec) {
	rowSize := len(self)
	for i := 0; i < rowSize; i++ {
		self[i][colIdx] = v[i]
	}
}

// selfとotherの積を返す.
//
// selfが(n, m)型の行列でotherが(m, l)型の行列であれば,
// self.Prod(other)は(n, l)型の行列である.
func (self Mat) Prod(other Mat) Mat {
	rowSize := len(self)
	colSize := len(other[0])
	untilProd := len(self[0])

	res := make([][]float64, len(self))
	for y := 0; y < rowSize; y++ {
		res[y] = make([]float64, len(other[0]))
		for x := 0; x < colSize; x++ {
			for k := 0; k < untilProd; k++ {
				res[y][x] += self[y][k] * other[k][x]
			}
		}
	}
	return Mat(res)
}

// ベクトルに行列を作用させて その結果を返す.
func (self Mat) Act(v Vec) Vec {
	size := len(self)
	sumUntil := len(v)

	res := make([]float64, size)
	for y := 0; y < size; y++ {
		for k := 0; k < sumUntil; k++ {
			res[y] += self[y][k] * v[k]
		}
	}
	return Vec(res)
}

// selfとotherのそれぞれの要素の差の絶対値の差の二乗の和を返す.
func (self Mat) Dist2(other Mat) float64 {
	rowSize := len(self)
	colSize := len(self[0])
	var s float64

	for y := 0; y < rowSize; y++ {
		for x := 0; x < colSize; x++ {
			s += (self[y][x] - other[y][x]) * (self[y][x] - other[y][x])
		}
	}

	return s
}

// selfとotherのそれぞれの要素の差の絶対値が1.0e-4より小さいかどうかを返す.
func (self Mat) Close(other Mat) bool {
	for i, _ := range self {
		if !CloseS(self[i], other[i]) {
			return false
		}
	}

	return true
}

// size * sizeの単位行列を作成する.
func E(size int) Mat {
	mat := ZeroMat(size, size)
	for i := 0; i < size; i++ {
		mat[i][i] = 1.0
	}
	return mat
}

func validateMat(mat [][]float64) error {
	// 全ての列の長さは同じ
	col_len := len(mat[0])
	for _, row := range mat[1:] {
		if len(row) != col_len {
			return fmt.Errorf("length of %v is not %v", row, col_len)
		}
	}

	return nil
}

// ゼロ行列を作成する.
func ZeroMat(rowSize int, colSize int) Mat {
	mat := make([][]float64, rowSize)
	for i := 0; i < rowSize; i++ {
		mat[i] = make([]float64, colSize)
	}

	return mat
}

// 行列のコピーを作成する.
func CopyMat(mat Mat) Mat {
	size := len(mat)
	dest := make([][]float64, size)
	for i, row := range mat {
		dest[i] = make([]float64, size)
		copy(dest[i], row)
	}
	return Mat(dest)
}

// rowSize * colSizeの行列を作成する.
func RandMat(rowSize, colSize int) Mat {
	mat := make([][]float64, rowSize)
	for y := 0; y < rowSize; y++ {
		mat[y] = make([]float64, colSize)
		for x := 0; x < colSize; x++ {
			mat[y][x] = rand.Float64()
		}
	}

	return Mat(mat)
}
