package entity

import (
	"reflect"
	"testing"
)

func Test_ZeroVec(t *testing.T) {
	got := ZeroVec(3)
	want := Vec{0.0, 0.0, 0.0}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %T: %v", got, got)
		t.Errorf("want: %T: %v", want, want)
	}
}

func Test_Vec_Sub(t *testing.T) {
	v1 := Vec([]float64{2.0, 3.0, 5.0})
	v2 := Vec([]float64{7.0, 11.0, 13.0})

	got := v1.Sub(v2)
	want := Vec([]float64{2.0 - 7.0, 3.0 - 11.0, 5.0 - 13.0})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %T: %v", got, got)
		t.Errorf("want: %T: %v", want, want)
	}
}

func Test_Vec_Dist2(t *testing.T) {
	v := Vec([]float64{2.0, 3.0, 5.0})
	v2 := Vec([]float64{7.0, 11.0, 13.0})
	got := v.Dist2(v2)
	want := 153.0

	if got != want {
		t.Errorf("Actual: %v", got)
		t.Errorf("Expected: %v", want)
	}
}

func Test_Vec_Close(t *testing.T) {
	cases := []struct {
		v1, v2 Vec
		want   bool
	}{
		{
			v1:   Vec([]float64{0.0, 0.0}),
			v2:   Vec([]float64{0.0, 0.0}),
			want: true,
		},
		{
			v1:   Vec([]float64{0.0, 0.0}),
			v2:   Vec([]float64{0.0, 1.0e-5}),
			want: true,
		},
		{
			v1:   Vec([]float64{0.0, 0.0}),
			v2:   Vec([]float64{0.0, 1.0e-3}),
			want: false,
		},
	}

	for _, c := range cases {
		if !(c.v1.Close(c.v2) == c.want) {
			t.Errorf("Fail Test. v1=%v v2=%v want=%v", c.v1, c.v2, c.want)
		}
	}
}

func Test_CopyVec(t *testing.T) {
	v := Vec([]float64{2.0, 1.0, 3.0})
	v2 := CopyVec(v)
	v2[1] = 100.0

	want_v := Vec([]float64{2.0, 1.0, 3.0})
	want_v2 := Vec([]float64{2.0, 100.0, 3.0})

	if !reflect.DeepEqual(v, want_v) {
		t.Errorf("got v: %T: %v", v, v)
		t.Errorf("want v: %T: %v", want_v, want_v)
	}
	if !reflect.DeepEqual(v2, want_v2) {
		t.Errorf("got v2: %T: %v", v2, v2)
		t.Errorf("want v2: %T: %v", want_v2, want_v2)
	}
}

func Test_RandVec(t *testing.T) {
	v := RandVec(3)

	for i := 0; i < 3; i++ {
		if v[i] <= 0 || v[i] >= 1 {
			t.Errorf("Error: i=%v val=%v", i, v[i])
		}
	}
}
