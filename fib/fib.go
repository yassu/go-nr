// 開発のサンプル用のパッケージ.
//
// 開発方法を考えるために用いる.
package fib

// フィボナッチ数列の第n項目を求める.
//
// nには0以上の整数を入力すること.
func Fib(n int) int {
	a, b := 0, 1
	for i := 0; i < n; i++ {
		a, b = b, a+b
	}

	return a
}
